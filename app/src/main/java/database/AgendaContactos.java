package database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

public class AgendaContactos {

    private Context context;
    private AgendaDbHelper mDbHelper;
    private SQLiteDatabase db;
    private String[] columnsToRead = new String[]{
            DefinirTabla.Contacto._ID,
            DefinirTabla.Contacto.NOMBRE, DefinirTabla.Contacto.TELEFONO1,
            DefinirTabla.Contacto.TELEFONO2,
            DefinirTabla.Contacto.DOMICILIO,
            DefinirTabla.Contacto.NOTAS,
            DefinirTabla.Contacto.FAVORITO
    };

    public AgendaContactos(Context context) {
        this.context = context;
        mDbHelper = new AgendaDbHelper(this.context);

    }

    public void openDatabase() {
        db = mDbHelper.getWritableDatabase();
    }

    public long insertContacto(Contacto c) {
        ContentValues values = new ContentValues();

        Log.println(Log.INFO,"domicilio", c.getDomicilio());

        values.put(DefinirTabla.Contacto.NOMBRE, c.getNombre());
        values.put(DefinirTabla.Contacto.TELEFONO1, c.getTelefono1());
        values.put(DefinirTabla.Contacto.TELEFONO2, c.getTelefono2());
        values.put(DefinirTabla.Contacto.DOMICILIO, c.getDomicilio());
        values.put(DefinirTabla.Contacto.NOTAS, c.getNotas());
        values.put(DefinirTabla.Contacto.FAVORITO, c.getFavorito());
        return db.insert("contactos", null, values); //regresa el id insertado

    }

    public long UpdateContacto(Contacto c, long id) {
        ContentValues values = new ContentValues();

        values.put(DefinirTabla.Contacto.NOMBRE, c.getNombre());
        values.put(DefinirTabla.Contacto.TELEFONO1, c.getTelefono1());
        values.put(DefinirTabla.Contacto.TELEFONO2, c.getTelefono2());
        values.put(DefinirTabla.Contacto.DOMICILIO, c.getDomicilio());
        values.put(DefinirTabla.Contacto.NOTAS, c.getNotas());
        values.put(DefinirTabla.Contacto.FAVORITO, c.getFavorito());
        String criterio = DefinirTabla.Contacto._ID + " = " + id;
        return db.update(DefinirTabla.Contacto.TABLE_NAME, values, criterio, null); //regresa el id insertado
    }

    public int deleteContacto(long id) {
        String criterio = DefinirTabla.Contacto._ID + " = " + id;
        return db.delete(DefinirTabla.Contacto.TABLE_NAME, DefinirTabla.Contacto._ID + "=?", null);
    }

    private Contacto readContacto(Cursor cursor) {
        Contacto c = new Contacto();
        c.set_ID(cursor.getInt(0));
        c.setNombre(cursor.getString(1));
        c.setTelefono1(cursor.getString(2));
        c.setTelefono2(cursor.getString(3));
        c.setDomicilio(cursor.getString(4));
        c.setNotas(cursor.getString(5));
        c.setFavorito(cursor.getInt(6));
        return c;

    }


    public Contacto getContacto(long id) {
        Contacto contacto =null;
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        Cursor c = db.query(DefinirTabla.Contacto.TABLE_NAME,
                columnsToRead, DefinirTabla.Contacto._ID + " = ?", new String[]{String.valueOf(id)}, null,
                null,
                null
        );
        if (c.moveToFirst()) contacto = readContacto(c);
        c.close();
        return contacto;
    }
    public ArrayList<Contacto> allContactos(){
        Cursor cursor = db.query(DefinirTabla.Contacto.TABLE_NAME, columnsToRead, null, null, null, null, null);

        ArrayList<Contacto> contactos = new ArrayList<Contacto>(); cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            Contacto c = readContacto(cursor);
            contactos.add(c);
            cursor.moveToNext();
        }
        cursor.close();
        return contactos;

}

    public void close(){
        mDbHelper.close(); }

}
