package com.example.agenda_sqlite;

import androidx.appcompat.app.AppCompatActivity;

import database.AgendaContactos;
import database.AgendaDbHelper;
import database.Contacto;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView lblRespuesta;
    private Button btnRespuesta;
    private EditText txtId;
    private Button btnBuscar;
    private  TextView lblNombre;
    private AgendaContactos db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        db = new AgendaContactos(MainActivity.this);

        lblRespuesta = (TextView) findViewById(R.id.lblRespuesta);
        btnRespuesta = (Button) findViewById(R.id.btnProbar);
        txtId = (EditText) findViewById(R.id.txtId);
        btnBuscar = (Button) findViewById(R.id.btnBuscar);
        lblNombre= (TextView) findViewById(R.id.lblNombre);

        btnRespuesta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Contacto c = new Contacto(0,"jose","9999","999","av del mar 12","notas",0);
                db.openDatabase();
               long idx= db.insertContacto(c);
               lblRespuesta.setText("se agrego con id" + idx);
             db.close();


            }
        });
        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Contacto con;
                long id = Long.parseLong(txtId.getText().toString());
                db.openDatabase();
                con= db.getContacto(id);
                if(con==null) lblNombre.setText("No se encontro");
               else  lblNombre.setText(con.getNombre());
                db.close();



            }
        });
    }
}
